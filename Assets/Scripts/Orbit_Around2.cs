﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbit_Around2 : MonoBehaviour {

    public GameObject Origin_gamObj1;
    float speed = 30;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Orbit();

    }

    void Orbit()
    {
        transform.RotateAround(Origin_gamObj1.transform.position, Vector3.up, speed * Time.deltaTime);
       
    }
}
