﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Object_Clicker : MonoBehaviour {

    public GameObject sphere1;
    public GameObject sphere2;
    public GameObject sphere3;

    public Transform myChildObject;   

   


    // Use this for initialization
    public void Start ()
    {
    }

    // Update is called once per frame
    public void Update ()
    {       

        if (Input.GetMouseButtonDown(0))
        {            
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 500.0f))
            {
                if (hit.transform != null)
                {                  

                    PrintName(hit.transform.gameObject);                    
                }
            }
        }
        
	}

    public void PrintName(GameObject go)
    {
        print(go.name);

        if (go.name == "Sphere1")
        {
            DontDestroyOnLoad(go.gameObject);
            //SceneManager.LoadScene("Scene3");
            Destroy(sphere2);
            Destroy(sphere3);

        }

        else if (go.name == "Sphere2")
        {
            go.transform.position = new Vector3(0, 0, 0);

            DontDestroyOnLoad(go.gameObject);
            //SceneManager.LoadScene("Scene3");
            Destroy(sphere1);
            Destroy(sphere3);
        }


        else if (go.name == "Sphere3")
        {
            myChildObject.parent = null;

            DontDestroyOnLoad(go.gameObject);
            go.transform.position = new Vector3(0, 0, 0);

            Destroy(sphere1);
            Destroy(sphere2);
        }       

    }
}
