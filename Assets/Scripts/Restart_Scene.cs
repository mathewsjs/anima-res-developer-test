﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart_Scene : MonoBehaviour {

	// Added to the timeline button on top

    public void scene_one()
    {
        SceneManager.LoadScene("Scene1");
    }

    public void scene_two()
    {
        SceneManager.LoadScene("Scene2");
    }

    public void scene_three()
    {
        SceneManager.LoadScene("Scene3");
    }
}
