﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detach_Child : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

    public Transform myChildObject;
    bool detachChild = true;

    // Update is called once per frame
    void Update ()
    {
        if (detachChild == true)
        {
            myChildObject.parent = null;
        }

    }
}
