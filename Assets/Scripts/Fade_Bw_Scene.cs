﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Fade_Bw_Scene : MonoBehaviour
{
    public Animator animator;


    // Update is called once per frame
    void Update ()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Fadetolevel(1);

        }
    }

    public void Fadetolevel(int levelIndex)
    {
        animator.SetTrigger("FadeOut");
    }

    public void Load_Scene_two()
    {
        SceneManager.LoadScene(1);
    }

    public void Load_Scene_three()
    {
        SceneManager.LoadScene(2);
    }

}
