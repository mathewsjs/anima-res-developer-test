﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInScript : MonoBehaviour {

    MeshRenderer rend;
    public GameObject cube;

	// Use this for initialization
	void Start () {
        rend = cube.GetComponent<MeshRenderer>();
        Color c = rend.material.color;
        c.a = 0f;
        rend.material.color = c;
	}
    IEnumerator FadeIn()
    {
        for(float f = 0.05f; f<=1; f += 0.05f)
        {
            Color c = rend.material.color;
            c.a = f;
            rend.material.color = c;
            yield return new WaitForSeconds(0.3f);
        }
    }

    
	/*
    public void StartFading()
    {
        StartCoroutine("FadeIn");
    }
    */

    void Update()
    {
        StartCoroutine("FadeIn");
    }
     
       
}
